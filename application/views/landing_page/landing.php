<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WEBKU</title>
    <link rel="shortcut icon" href="<?= base_url() ?>assets/imges/icons/logo.svg" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.css" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet" />
</head>

<body>
    <!-- nav -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-transparent position-absolute w-100">
        <div class="container mt-4">
            <a class="navbar-brand" href="index.html"><img src="<?= base_url() ?>assets/imges/logo.png" alt="logo" width="50%" class="d-inline-block align-top" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#paket">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#benefit">Benefit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('user') ?>">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end nav -->

    <!-- hero -->
    <section id="hero">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-12 hero-tagline mx-auto">
                    <h1>Jasa Bikin Website Sesuai Kebutuhan Anda</h1>
                    <p>Webku sudah dipercaya 100++ perusahaan untuk bikin website terbaik</p>
                    <a href="#paket"><button class="tombol-big rounded-2 p-2">Pesan Sekarang</button></a>
                </div>
            </div>
            <img src="<?= base_url() ?>assets/imges/gambar.png" alt="gambar" class="img-hero position-absolute top-50 start-50 translate-middle-x" />
            <img src="<?= base_url() ?>assets/imges/ornamen.png" alt="gambar" width="100%" class="ornamen-img position-absolute start-0 top-0" />
        </div>
    </section>
    <!-- end hero -->

    <!-- alasan -->
    <section id="alasan">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Mengapa Bisnis Anda Wajib Memiliki Website?</h2>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-3 text-center">
                    <div class="card-alasan mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/kredible.svg" alt="kredibilitas" class="p-4 position-absolute top-50 start-50 translate-middle-x" />
                        </div>
                        <h3>Meningkatkan Kredibilitas</h3>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="card-alasan mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/target.svg" alt="pemasaran" width="90%" class="position-absolute top-50 start-50 translate-middle-x" />
                        </div>
                        <h3>Media Pemasaran</h3>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="card-alasan mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/informasi.svg" alt="informasi" width="90%" class="position-absolute top-50 start-50 translate-middle-x" />
                        </div>
                        <h3>Media Informasi</h3>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="card-alasan mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/service.svg" alt="Layanan" width="90%" class="position-absolute top-50 start-50 translate-middle-x" />
                        </div>
                        <h3>Layanan Pelanggan</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end alasan -->

    <!-- client -->
    <section id="client">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Bagian Dari Client Kami:</h2>
                </div>
            </div>
            <div class="row mt-5 mb-2">
                <div class="col-md-3 position-relative">
                    <a href="https://www.gojek.com/"><img src="<?= base_url() ?>assets/imges/gojek.png" width="70%" alt="gojek" class="pt-4 position-absolute top-50 start-50 translate-middle" /></a>
                </div>
                <div class="col-md-3 position-relative">
                    <a href="https://shopee.co.id/"><img src="<?= base_url() ?>assets/imges/shopee.png" width="70%" alt="shopee" class="pt-4 position-absolute top-50 start-50 translate-middle" /></a>
                </div>
                <div class="col-md-3 position-relative">
                    <a href="https://www.dana.id/"><img src="<?= base_url() ?>assets/imges/dana.png" width="70%" alt="dana" class="pt-4 position-absolute top-50 start-50 translate-middle" /></a>
                </div>
                <div class="col-md-3 position-relative">
                    <a href="https://www.tokopedia.com/"><img src="<?= base_url() ?>assets/imges/tokopedia.png" width="70%" alt="tokopedia" class="pt-4 position-absolute top-50 start-50 translate-middle" /></a>
                </div>
            </div>
        </div>
    </section>
    <!-- end client -->

    <!-- testimoni -->
    <section id="testimoni">
        <div class="container test-bg">
            <div class="row">
                <div class="col mb-5 text-center">
                    <h2>Bagaimana Pendapat Client?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?= base_url() ?>assets/imges/testimo.png" class="d-block w-100" alt="testimo" />
                            </div>
                            <div class="carousel-item">
                                <img src="<?= base_url() ?>assets/imges/testimo2.png" class="d-block w-100" alt="testimo" />
                            </div>
                            <div class="carousel-item">
                                <img src="<?= base_url() ?>assets/imges/testimo3.png" class="d-block w-100" alt="testimo" />
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end testimoni -->

    <!-- portofolio -->
    <section id="portofolio">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Website Yang Pernah Kami Buat:</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center">
                    <a href="#"><img src="<?= base_url() ?>assets/imges/image 1.png" alt="porto" class="img-porto" /></a>
                </div>
                <div class="col-md-6 text-center">
                    <a href="#"><img src="<?= base_url() ?>assets/imges/image 2.png" alt="porto" class="img-porto" /></a>
                </div>
                <div class="col-md-6 text-center">
                    <a href="#"><img src="<?= base_url() ?>assets/imges/image 3.png" alt="porto" class="img-porto" /></a>
                </div>
                <div class="col-md-6 text-center">
                    <a href="#"><img src="<?= base_url() ?>assets/imges/image 4.png" alt="porto" class="img-porto" /></a>
                </div>
            </div>
        </div>
    </section>
    <!-- end portofolio -->

    <!-- benefit -->
    <div id="benefit">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Apa Keuntungan Yang Anda Dapat Dari Kami?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-benefit red-card mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/time.svg" alt="time" class="position-absolute pt-2" />
                        </div>
                        <h3>Hemat Waktu</h3>
                        <p>Anda dapat menghemat waktu untuk pengerjaan website sehingga anda bisa fokus ke bisnis anda.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-benefit yellow-card mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/tren.svg" alt="tren" class="position-absolute pt-2" />
                        </div>
                        <h3>Desain Kekinian</h3>
                        <p>Tampilan desain anda kekinian, menarik dan akan disukai oleh pengunjung anda.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-benefit red-card mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/responsif.svg" alt="responsif" class="position-absolute pt-2" />
                        </div>
                        <h3>Responsif</h3>
                        <p>Website anda tetap enak dilihat ketika dibuka di berbagai smartphone maupun desktop.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-benefit yellow-card mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/maintenance.svg" alt="maintenance" class="position-absolute pt-2" />
                        </div>
                        <h3>Gratis Maintenance</h3>
                        <p>Kami akan memberikan gratis maintenance error di website anda (jika masih berlangganan pada kami).</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-benefit red-card mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/team.svg" alt="team" class="position-absolute pt-2" />
                        </div>
                        <h3>Tim Berpengalaman</h3>
                        <p>Tim kami sudah berpengalaman membuat berbagai website yang sesuai dengan bisnis anda.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-benefit yellow-card mx-auto">
                        <div class="position-relative">
                            <img src="<?= base_url() ?>assets/imges/icons/online.svg" alt="online" class="position-absolute pt-2" />
                        </div>
                        <h3>Selalu Online</h3>
                        <p>Website anda pastinya akan selalu online. Apabila ada masalah akan kami cek.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end benefit -->

    <!-- paket -->
    <div id="paket">
        <div class="position-relative">
            <img src="<?= base_url() ?>assets/imges/ornamen-paket.png" alt="gambar" width="100%" class="ornamen-img position-absolute" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Pilih Paket Website Design:</h2>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-4">
                    <div class="card-paket" height="574px">
                        <div class="judul text-center">
                            <h3>Basic</h3>
                            <p>Rp 5.432.100</p>
                            <h4>Rp 1.234.500</h4>
                        </div>
                        <ul style="list-style-image: url(<?= base_url() ?>assets/imges/icons/check.svg); color: rgba(30, 26, 86, 1)">
                            <li>Gratis Domain SSL</li>
                            <li>1 GB Space</li>
                            <li>5 GB Bandwidth per Bulan</li>
                            <li>1 Email Bisnis</li>
                            <li>Design Website Premium</li>
                            <li>1 Halaman Website</li>
                            <li>2 Kali Revisi</li>
                            <li>Optimasi Website</li>
                            <li>Optimasi Search Engine Goolge</li>
                            <li>Responsive Design</li>
                            <li>Anti Spam Hacker</li>
                            <li>Whatsapp Chat Premium</li>
                            <li>1 Design Banner Website</li>
                        </ul>
                        <a href="<?= base_url('user') ?>"><button class="tombol-small rounded-2 pb-1">Pesan Sekarang</button></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-paket" height="635px">
                        <div class="judul text-center">
                            <h3>Pro</h3>
                            <p>Rp 9.876.500</p>
                            <h4>Rp 5.678.900</h4>
                        </div>
                        <ul style="list-style-image: url(<?= base_url() ?>assets/imges/icons/check.svg); color: rgba(30, 26, 86, 1)">
                            <li>Gratis Domain SSL</li>
                            <li>3 GB Space</li>
                            <li>10 GB Bandwidth per Bulan</li>
                            <li>5 Email Bisnis</li>
                            <li>Design Website Premium</li>
                            <li>7 Halaman Website</li>
                            <li>5 Kali Revisi</li>
                            <li>Optimasi Website</li>
                            <li>Optimasi Search Engine Goolge</li>
                            <li>Responsive Design</li>
                            <li>Anti Spam Hacker</li>
                            <li>Whatsapp Chat Premium</li>
                            <li>Blog Premium</li>
                            <li>3 Pilihan Design Logo</li>
                            <li>3 Design Benner Website</li>
                        </ul>
                        <a href="<?= base_url('user') ?>"><button class="tombol-small rounded-2 pb-1">Pesan Sekarang</button></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-paket" height="917px">
                        <div class="judul text-center">
                            <h3>Super</h3>
                            <p>Rp 21.098.700</p>
                            <h4>Rp 8.901.900</h4>
                        </div>
                        <ul style="list-style-image: url(<?= base_url() ?>assets/imges/icons/check.svg); color: rgba(30, 26, 86, 1)">
                            <li>Gratis Domain SSL</li>
                            <li>10 GB Space</li>
                            <li>30 GB Bandwidth per Bulan</li>
                            <li>20 Email Bisnis</li>
                            <li>Design Website Premium</li>
                            <li>20 Halaman Website</li>
                            <li>10 Kali Revisi</li>
                            <li>Optimasi Website</li>
                            <li>Optimasi Search Engine Goolge</li>
                            <li>Responsive Design</li>
                            <li>Anti Spam Hacker</li>
                            <li>Whatsapp Chat Premium</li>
                            <li>Blog Premium</li>
                            <li>Upload Max 150 Produk</li>
                            <li>Install Hitung Ongkir Otomatis</li>
                            <li>Install Auto Konfirmasi Pembayaran</li>
                            <li>Integrasi Payment Gateway</li>
                            <li>Pembuatan Fanpage Facebook</li>
                            <li>Pembuatan Instagram Bisnis</li>
                            <li>Pembuatan Youtube Channel</li>
                            <li>5 Artikel Original SEO Friendly 1000 Kata</li>
                            <li>6 Pilihan Design Logo</li>
                            <li>6 Design Banner Website</li>
                        </ul>
                        <a href="<?= base_url('user') ?>"><button class="tombol-small rounded-2 pb-1">Pesan Sekarang</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end paket -->

    <!-- footer -->
    <div id="footer">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-center mx-auto mt-4">
                        <p>Social Media Management | Branding & Design | Google Ads | Web Development | Search Engine Optimization | Portfolio</p>
                    </div>
                    <div class="col-md-10">
                        <div>
                            <img src="<?= base_url() ?>assets/imges/icons/logo.svg" alt="logo" width="143px" height="65px" />
                            <p class="ms-2">Webku merupakan perusahaan penyedia jasa pembuatan website</p>
                        </div>
                        <div>
                            <img src="<?= base_url() ?>assets/imges/icons/location.svg" alt="lokasi" width="65px" />
                            <span>Jl. Engku Putri No.100 Tampan, Pekanbaru Indonesia</span>
                        </div>
                    </div>
                    <div class="col-md-2 p-2" style="margin-top: 50px">
                        <div>
                            <a href="https://instagram.com/"><img src="<?= base_url() ?>assets/imges/icons/instagram.svg" alt="kontak" width="50px" /></a>
                            <a href="https://www.linkedin.com/"><img src="<?= base_url() ?>assets/imges/icons/linkedin.svg" alt="kontak" width="50px" /></a>
                        </div>
                        <div class="pt-2">
                            <a href="https://www.whatsapp.com"><img src="<?= base_url() ?>assets/imges/icons/whatsapp.svg" alt="kontak" width="50px" /></a>
                            <a href="https://www.google.com/gmail"><img src="<?= base_url() ?>assets/imges/icons/gmail.svg" alt="kontak" width="50px" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" style="font-weight: 400">
                <hr />
                <img src="<?= base_url() ?>assets/imges/icons/copyright.svg" alt="kontak" width="30px" />
                <span>Copyright Webku <?= date('Y'); ?></span>
            </div>
        </div>
    </div>
    <!-- end footer -->
    <script src="<?= base_url() ?>assets/js/jquery.slim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
</body>

</html>