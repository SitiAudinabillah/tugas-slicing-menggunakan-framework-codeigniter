<?php $this->load->view('templates/user_header'); ?>

<body>
    <div class="container">
        <img src="<?= base_url() ?>assets/imges/Intersect.png" alt="gambar" class="ornamen-img-login1 position-absolute start-0" />
        <img src="<?= base_url() ?>assets/imges/Intersect2.png" alt="gambar" class="ornamen-img-login2 position-absolute top-0" />
        <img src="<?= base_url() ?>assets/imges/Intersect3.png" alt="gambar" class="ornamen-img-login3 position-absolute" />
        <div class="row">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card-login position-absolute">
                        <div class="d-flex">
                            <div class="w-100">
                                <h3 class="mb-4 text-center">Login</h3>
                            </div>
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                        <form class="signin-form" method="post" action="<?= base_url('user') ?>">
                            <div class="form-group mb-3">
                                <label class="label" for="name">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
                                <?= form_error('email', '<small class="text-danger pl-3">', '</small> '); ?>
                            </div>
                            <div class="form-group mb-3">
                                <label class="label" for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <?= form_error('password', '<small class="text-danger pl-3">', '</small> '); ?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control btn rounded submit px-3" style="background: #874ce5; color: #fffcfc">Login</button>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-50 text-left">
                                    <label class="checkbox-wrap mb-0">Ingat Saya
                                        <input type="checkbox" checked>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="w-50 text-md-right">
                                    <a href="#">Lupa Password</a>
                                </div>
                            </div>
                        </form>
                        <p class="text-center">Belum Punya Akun? <a data-toggle="tab" href="<?= base_url('user/register') ?>" style="color: #874ce5;">Register</a></p>
                        <hr />
                        <span class="alternatif"> atau daftar dengan </span>
                        <div class="gf">
                            <a class="ml-5" href="#"><img src="<?= base_url() ?>assets/imges/icons/google.svg" alt="gambar" /></a>
                            <a class="ml-5" href="#"><img src="<?= base_url() ?>assets/imges/icons/facebook.svg" alt="gambar" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card-login-intro position-absolute">
                <a class="navbar-brand" href="index.html"><img src="<?= base_url() ?>assets/imges/icons/logo.svg" alt="logo" width="100%" class="mt-3 mb-5 d-inline-block align-top" /></a>
                <h1 style="color: #fff;">Jasa Bikin Website Sesuai Kebutuhan Anda</h1>
                <p>Webku sudah dipercaya 100++ perusahaan untuk bikin website terbaik</p>
            </div>
        </div>
    </div>
    </div>
    <?php $this->load->view('templates/user_footer'); ?>